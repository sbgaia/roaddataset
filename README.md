# RoAD: Robotic Arm Dataset

This repository contains the code and dataset for the paper "Robotic Arm Dataset (RoAD): a Dataset to Support the Design and Test of Anomaly Detection in a Production Line". The dataset is designed to support the design, development, and testing of anomaly detection systems in the context of production lines, as well as other applications involving robotic arms.

## Installation

You can install this package using pip. Run the following command:

```
pip install git+https://gitlab.com/AlessioMascolini/roaddataset/
```

## Usage

Once installed, you can use the `Dataset` class from the `RoadDataset` package. This class contains five subsets namely: 'training', 'collision', 'control', 'weight', and 'velocity', as described in the paper. 

Here's a simple example on how to import and use the `Dataset` class:

```python
from RoadDataset import Dataset

# Instantiate the Dataset
dataset = Dataset()

# Access a subset
training_subset = dataset['training']
```

## Normalization

The `Dataset` class comes with a normalization feature which employs minmax normalization technique and is enabled by default. To use this feature, pass a truthy value to the `normalize` parameter when creating an instance of the `Dataset` class:

```python
# Instantiate the Dataset with normalization
normalized_dataset = Dataset(normalize=True)
```

In addition, an instance of the `sklearn.preprocessing.MinMaxScaler` class is accessible via the `normalizer` attribute of the `Dataset` class, which can be used for further transformations if required.

```python
# Access the sklearn normalizer
normalizer = normalized_dataset.normalizer
```

## Citation

If you find this dataset useful for your research, please cite our paper.

---
